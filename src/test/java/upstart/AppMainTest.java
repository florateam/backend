package upstart;

import java.util.List;

import org.hibernate.Hibernate;
import org.joda.time.LocalDate;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import com.projectflow.spring.configuration.AppConfig;
import com.projectflow.spring.model.user.User;
import com.projectflow.spring.service.DaoService;

import junit.framework.TestCase;


@Transactional
public class AppMainTest extends TestCase {
	
	/*
	 * Tests if the application current configuration schema is working
	 * It also does a basic look up and inserts a  user in a table
	 * Good to check for StartUp errors caused by bad configuration
	 */
	@Test
	public void testApplicationUpstartConfiguration () {

		AbstractApplicationContext context = new AnnotationConfigApplicationContext(
				AppConfig.class);

		@SuppressWarnings("unchecked")
		DaoService<User> service = (DaoService<User>) context.getBean("userService");
		
		/*
		 * Create 
		 */
		User user = new User();
		user.setFirstName("Nelson");
		user.setLastName("Menetti");
		user.setEmail("nelsonmenetti@gmail");
		user.setCreatedOn(new LocalDate(2010, 10, 10));
		
		
		Hibernate.initialize(user.getGroups());
		/*
		 * Persist
		 */
		service.save(user);

		/*
		 * Get all employees list from database
		 */
		List<User> users = service.findAll();
		for (User item : users) {
			System.out.println(item);
		}

		/*
		 * delete an employee
		 */
		service.delete(user);

		context.close();
	}
}
