package com.projectflow.security.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.projectflow.spring")
public class AppConfig {

}
