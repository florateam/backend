package com.projectflow.spring.model.message;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import com.projectflow.spring.model.Model;
import com.projectflow.spring.model.user.User;


@Entity
@Table(name="Message")
public class Message extends Model {

	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
 
    @Column(name = "content")
    private String content;

    @Column(name = "createdOn", nullable = false )
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate createdOn;

    @Column(name = "updatedOn", nullable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate updatedOn;

    @OneToOne (cascade = CascadeType.ALL , fetch=FetchType.EAGER  )
    @PrimaryKeyJoinColumn
    private User user;
        
    @OneToMany (fetch =FetchType.LAZY )
    private Collection<Attachment> attachments;
    
	

}
