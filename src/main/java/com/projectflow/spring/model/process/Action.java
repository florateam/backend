package com.projectflow.spring.model.process;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.projectflow.spring.model.Model;
import com.projectflow.spring.model.message.Message;
import com.projectflow.spring.model.user.CostCenter;
import com.projectflow.spring.model.user.Department;
import com.projectflow.spring.model.user.User;


@Entity
@Table(name="action")
public class Action extends Model {

	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
 
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "order", nullable = false)
    private Long order;
        
    @JoinColumn (name ="id_parent_step", table="step" , referencedColumnName="id")
    private Long id_parent_step;

    @ManyToOne(cascade = CascadeType.ALL , fetch = FetchType.LAZY)
    @JoinColumn (name ="id_sla" , table="SLA" , referencedColumnName="id")
    private SLA sla;


    @OneToOne(cascade = CascadeType.ALL , fetch = FetchType.LAZY)
    @JoinColumn (name ="id_cost_center" , table="cost_center" , referencedColumnName="id")
    private CostCenter costCenter;
    
    @ManyToOne (fetch= FetchType.LAZY , cascade = CascadeType.ALL)
    private Department department;

    @ManyToOne (fetch= FetchType.LAZY , cascade = CascadeType.ALL)
    private AttributionRule attributionRule;

    @OneToOne 
    private User responsibleUser;
    
    
    //**REFER TO JPA 2.0 SPEC - THIS IS A ONE-TO-MANY MANY-TO-ONE Corner case
    // http://jcp.org/aboutJava/communityprocess/final/jsr317/index.html
    // ALL ENTITIES MUST BE PERSISTED BEFORE TRANSACTION COMMIT
    //It is always the programmer's responsibility to update both sides of a bidirectional relationship
    @ManyToOne
    private Action parent;
    
    @OneToMany(mappedBy="parent")
    private Collection<Action> children;

    @OneToMany
    private Collection<Message> messages;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getOrder() {
		return order;
	}

	public void setOrder(Long order) {
		this.order = order;
	}

	public Step getStepParent() {
		return stepParent;
	}

	public void setStepParent(Step stepParent) {
		this.stepParent = stepParent;
	}

	public SLA getSla() {
		return sla;
	}

	public void setSla(SLA sla) {
		this.sla = sla;
	}

	public CostCenter getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(CostCenter costCenter) {
		this.costCenter = costCenter;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public AttributionRule getAttributionRule() {
		return attributionRule;
	}

	public void setAttributionRule(AttributionRule attributionRule) {
		this.attributionRule = attributionRule;
	}

	public User getResponsibleUser() {
		return responsibleUser;
	}

	public void setResponsibleUser(User responsibleUser) {
		this.responsibleUser = responsibleUser;
	}

	public Action getParent() {
		return parent;
	}

	public void setParent(Action parent) {
		this.parent = parent;
	}

	public Collection<Action> getChildren() {
		return children;
	}

	public void setChildren(Collection<Action> children) {
		this.children = children;
	}

	public Collection<Message> getMessages() {
		return messages;
	}

	public void setMessages(Collection<Message> messages) {
		this.messages = messages;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attributionRule == null) ? 0 : attributionRule.hashCode());
		result = prime * result + ((children == null) ? 0 : children.hashCode());
		result = prime * result + ((costCenter == null) ? 0 : costCenter.hashCode());
		result = prime * result + ((department == null) ? 0 : department.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		result = prime * result + ((messages == null) ? 0 : messages.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		result = prime * result + ((responsibleUser == null) ? 0 : responsibleUser.hashCode());
		result = prime * result + ((sla == null) ? 0 : sla.hashCode());
		result = prime * result + ((stepParent == null) ? 0 : stepParent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Action other = (Action) obj;
		if (attributionRule == null) {
			if (other.attributionRule != null)
				return false;
		} else if (!attributionRule.equals(other.attributionRule))
			return false;
		if (children == null) {
			if (other.children != null)
				return false;
		} else if (!children.equals(other.children))
			return false;
		if (costCenter == null) {
			if (other.costCenter != null)
				return false;
		} else if (!costCenter.equals(other.costCenter))
			return false;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (messages == null) {
			if (other.messages != null)
				return false;
		} else if (!messages.equals(other.messages))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!order.equals(other.order))
			return false;
		if (parent == null) {
			if (other.parent != null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;
		if (responsibleUser == null) {
			if (other.responsibleUser != null)
				return false;
		} else if (!responsibleUser.equals(other.responsibleUser))
			return false;
		if (sla == null) {
			if (other.sla != null)
				return false;
		} else if (!sla.equals(other.sla))
			return false;
		if (stepParent == null) {
			if (other.stepParent != null)
				return false;
		} else if (!stepParent.equals(other.stepParent))
			return false;
		return true;
	}

    
	
}
