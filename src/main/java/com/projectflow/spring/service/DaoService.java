package com.projectflow.spring.service;

import java.util.List;

public interface DaoService<T> {

	void save(T object );
	
	void update(T object);

	void delete(T object);
	
	List<T> findAll();

	List<T> findByObjectField(T object);

	void deleteById(Long id);

	T findById(Long id);

}
