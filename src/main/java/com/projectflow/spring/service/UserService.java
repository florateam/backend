package com.projectflow.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.projectflow.spring.dao.UserDao;
import com.projectflow.spring.model.user.User;

@Service("userService")
@Transactional
public class UserService implements DaoService<User> {

	@Autowired
    private UserDao dao;
	

	@Override
	public void save(User object) {
		dao.save(object);
	}

	@Override
	public void update(User object) {
		dao.update(object);		
	}

	@Override
	public void delete(User object) {
		dao.delete(object);	
		
	}

	@Override
	public void deleteById(Long id) {
		dao.deleteById(id);
		
	}

	@Override
	public User findById(Long id) {
		return (User) dao.findById(id);
	}

	@Override
	public List<User> findAll() {
		return dao.findAll();
	}

	@Override
	public List<User> findByObjectField(User object) {
		return dao.findByObjectField(object);
	}


}
