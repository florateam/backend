package com.projectflow.spring.dao;

import java.util.List;

public  interface Dao<T> {

	void save(T object );
	
	void update(T object);

	void delete(T object);

	void deleteById(Long id);

	T findById(Long id);
	
	List<T> findAll();

	List<T> findByObjectField(T object);

}
