package com.projectflow.spring.dao;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Component;

@Component
public abstract class DaoAbstractImpl<T> implements Dao<T> {
	
	private final Class<T> genericType;
	
    
    @SuppressWarnings("unchecked")
    public DaoAbstractImpl()
    {
        this.genericType = (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), DaoAbstractImpl.class);
    }
    
    @Autowired
    private SessionFactory sessionFactory;
 
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
 
    public void persist(Object entity) {
        getSession().persist(entity);
    }
 
    @Override
    public void delete(T entity) {
        getSession().delete(entity);
    }
    
    @Override
	public void save(T object) {
        persist(object);
    }
 
    @Override
    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        Criteria criteria = getSession().createCriteria(genericType);
        return (List<T>) criteria.list();
    }
    
    @Override
    public void deleteById(Long id) {
        Query query = getSession().createSQLQuery("delete from "+ genericType+ "where id = :id");
        query.setLong("id", id);
        query.executeUpdate();
    }
 
    @Override
    @SuppressWarnings("unchecked")
    public T findById(Long id){
        Criteria criteria = getSession().createCriteria(genericType);
        criteria.add(Restrictions.eq("id",id));
        return (T) criteria.uniqueResult();
    }
    @Override 
    public void update(T object){
        getSession().update(object);
    }

}
